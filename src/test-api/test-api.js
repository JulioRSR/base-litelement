import { LitElement, html } from "lit"

class TestApi extends LitElement {

    static get properties() {
        return {
            urlApi: {type: String},
            movies: {type: Array}
        };
    }

    constructor() {
        super();

        this.urlApi = 'https://swapi.dev/api/films';
        this.movies = [];
        this.getMoviesData();
    }

    getMoviesData() {
        let xhr = new XMLHttpRequest();
        xhr.onload = () => {
            if (xhr.status === 200) {
                let APIresponse = JSON.parse(xhr.responseText);
                this.movies = APIresponse.results;
            }
        }
        xhr.open('GET', this.urlApi);
        xhr.send();
        /* Other way to do this
        fetch(this.urlApi)
        .then(response => response.json())
        .then(data => this.movies = data.results);
        */
    }

    render() {
        return html`
            <h1>Test API</h1>
            ${this.movies.map(
                movie => html`<div>La película ${movie.title}, fue dirigida por ${movie.director}</div>`
            )}
        `;
    }
}

customElements.define('test-api', TestApi);