import { LitElement, html } from "lit"
import '../persona-main-dm/persona-main-dm.js';
import '../persona-card-list/persona-card-list.js';
import '../persona-form/persona-form.js';

class PersonaMain extends LitElement {

    static get properties() {
        return {
            people: {type: Array},
            displayAddPersonForm: {type: Boolean}
        };
    }

    constructor() {
        super();

        this.people = [];
        this.displayAddPersonForm = false;
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <h2 class="text-center">Personas</h2>
            <div class="container-fluid" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4"> 
                    ${this.people.map( 
                        person => html`<persona-card-list 
                            id="${person.name}" 
                            @delete-person="${this.deletePerson}"
                            @info-person="${this.infoPerson}"
                            .name="${person.name}" 
                            .yearsInCompany="${person.yearsInCompany}"
                            .profile="${person.profile}"
                            .avatar="${person.image}"
                        >
                        </persona-card-list>`
                    )}
                </div>
            </div>
            <div class="container-fluid">
                <div class="row">
                    <persona-form 
                        @persona-form-close="${this.personFormClose}"
                        @add-new-person="${this.addPerson}"
                        id="personaForm" 
                        class="d-none border rounded border-primary p-4"
                    >
                    </persona-form>
                </div>
            </div>
            <persona-main-dm @get-people-data="${this.getPeopleData}"></persona-main-dm>
        `;
    }

    updated(changedProperties) {
        if (changedProperties.has('displayAddPersonForm')) {
            if (this.displayAddPersonForm === true) {
                this.displayAddPersonFormData();
            } else {
                this.displayPersonList();
            }
        }
        if (changedProperties.has('people')) {
            this.dispatchEvent(
                new CustomEvent(
                    'updated-people',
                    {
                        detail: {
                            people: this.people
                        }
                    }
                )
            );
        }
    }

    getPeopleData(e) {
        this.people = e.detail.people;
    }

    personFormClose() {
        this.displayAddPersonForm = false;
    }

    displayAddPersonFormData() {
        this.shadowRoot.getElementById('peopleList').classList.add('d-none');
        this.shadowRoot.getElementById('personaForm').classList.remove('d-none');
    }

    displayPersonList() {
        this.shadowRoot.getElementById('personaForm').classList.add('d-none');
        this.shadowRoot.getElementById('peopleList').classList.remove('d-none');
    }

    addPerson(e) {
        if (e.detail.editPerson === true) {
            this.people = this.people.map(
                person => person.name === e.detail.person.name ? person = e.detail.person : person
            );
        } else {
            // JS spreed syntax
            this.people = [...this.people, e.detail.person]
        }
        this.displayAddPersonForm = false;
    }

    infoPerson(e) {
        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );
        this.shadowRoot.getElementById('personaForm').person = chosenPerson[0];
        this.shadowRoot.getElementById('personaForm').editPerson = true;
        this.displayAddPersonForm = true;
    }

    deletePerson(e) {
        this.people = this.people.filter(
            person => person.name !== e.detail.name
        );
    }
}

customElements.define('persona-main', PersonaMain);