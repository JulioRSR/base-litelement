import { LitElement, html } from "lit"

class PersonaForm extends LitElement {

    static get properties() {
        return {
            person: {type: Object},
            editPerson: {type: Boolean}
        };
    }

    constructor() {
        super();

        this.person = {}
        this.resetFormData();
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <div>
                <form id="addNewPerson">
                    <div class="form-group">
                        <label>Nombre completo *</label>
                        <input type="text" 
                            class="form-control" 
                            placeholder="Nombre completo" 
                            @input="${this.addName}"
                            .value="${this.person.name}" 
                            ?disabled="${this.editPerson}"
                            required>
                    </div>
                    <div class="form-group">
                        <label>Perfil *</label>
                        <textarea 
                            class="form-control" 
                            placeholder="Perfil" 
                            rows="5" 
                            @input="${this.addProfile}" 
                            .value="${this.person.profile}"
                            required>
                        </textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa *</label>
                        <input type="number" 
                            class="form-control" 
                            placeholder="Años en la empresa" 
                            @input="${this.addYearsInCompany}" 
                            .value="${this.person.yearsInCompany}" 
                            required>
                    </div>
                    <br>
                    <button class="btn btn-primary" @click="${this.backToList}">Atrás</button>
                    <button class="btn btn-success" @click="${this.savePerson}">Guardar</button>
                </form>
            </div>
        `;
    }

    backToList(e) {
        e.preventDefault();
        this.resetFormData();
        this.dispatchEvent(new CustomEvent('persona-form-close', {}));
    }

    resetFormData() {
        this.person = {
            name: '',
            profile: '',
            yearsInCompany: ''
        };
        this.editPerson = false;
    }

    addName(e) {
        this.person.name = e.target.value;
    }

    addProfile(e) {
        this.person.profile = e.target.value;
    }

    addYearsInCompany(e) {
        this.person.yearsInCompany = e.target.value;
    }

    savePerson(e) {
        e.preventDefault();
        this.dispatchEvent(
            new CustomEvent(
                'add-new-person', {
                    detail: {
                        person: {
                            name: this.person.name,
                            yearsInCompany: this.person.yearsInCompany,
                            profile: this.person.profile,
                            image: {
                                src: '/assets/images/people/01.png',
                                alt: this.person.name + ' photo'
                            }
                        },
                        editPerson: this.editPerson
                    }
                }
            )
        );
        this.resetFormData();
    }
}

customElements.define('persona-form', PersonaForm);