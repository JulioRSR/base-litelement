import { LitElement, html } from "lit"
import '../persona-header/persona-header.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-stats/persona-stats.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';

class PersonaApp extends LitElement {

    static get properties() {
        return {
            people: {type: Array}
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <persona-header></persona-header>
            <div class="container-fluid">
                <div class="row">
                    <persona-sidebar class="col-2" @new-person="${this.addPerson}"></persona-sidebar>
                    <persona-main class="col-10" @updated-people="${this.updatedPeople}"></persona-main>
                </div>
            </div>
            <persona-footer></persona-footer>
            <persona-stats @updated-people-stats="${this.updatedPeopleStats}"></persona-stats>
        `;
    }

    updated(changedProperties) {
        if (changedProperties.has('people')) {
            this.shadowRoot.querySelector('persona-stats').people = this.people;
        }
    }

    addPerson(e) {
        this.shadowRoot.querySelector('persona-main').displayAddPersonForm = true;
    }

    updatedPeople(e) {
        this.people = e.detail.people;
    }

    updatedPeopleStats(e) {
        this.shadowRoot.querySelector('persona-sidebar').peopleStats = e.detail.peopleStats;
    }
}

customElements.define('persona-app', PersonaApp);