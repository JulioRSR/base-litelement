import { LitElement, html } from "lit"

class PersonaCardList extends LitElement {

    static get properties() {
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            avatar: {type: Object},
            profile: {type: String}
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
            <div class="card h-100">
                <img src="${this.avatar.src}" class="card-img-top" alt="${this.avatar.alt}" loading="lazy">
                <div class="card-body">
                    <h5 class="card-title">${this.name}</h5>
                    <p class="card-text">${this.profile}</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">${this.yearsInCompany} años en la empresa</li>
                    </ul>                
                </div>
                <div class="card-footer">
                    <button class="btn btn-danger col-5" @click="${this.deletePerson}"><strong>Eliminar</strong></button>
                    <button class="btn btn-info col-5 offset-1" @click="${this.infoPerson}"><strong>Info</strong></button>
                </div>
            </div>
        `;
    }

    deletePerson(e) {
        this.dispatchEvent(
            new CustomEvent(
                'delete-person', {
                    'detail': {
                        'name': this.name
                    }
                }
            )
        );
    }

    infoPerson(e) {
        this.dispatchEvent(
            new CustomEvent(
                'info-person', {
                    detail: {
                        name: this.name,
                        profile: this.profile,
                        yearsInCompany: this.yearsInCompany
                    }
                }
            )
        )
    }
}

customElements.define('persona-card-list', PersonaCardList);