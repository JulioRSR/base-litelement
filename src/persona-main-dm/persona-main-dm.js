import { LitElement, html } from "lit"

class PersonaMainDM extends LitElement {

    static get properties() {
        return {
            people: {type: Array},
        };
    }

    constructor() {
        super();

        this.people = [
            {
                name: 'Elle Ripley',
                yearsInCompany: 10,
                image: {
                    src: '/assets/images/people/02.png',
                    alt: 'Elle Ripley photo'
                },
                profile: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec varius mauris mi, a efficitur orci mollis cursus.'
            }, {
                name: 'Bruce Banner',
                yearsInCompany: 2,
                image: {
                    src: '/assets/images/people/04.png',
                    alt: 'Bruce Banner photo'
                },
                profile: 'In sed sem quis ante euismod vestibulum et egestas ipsum. Sed dignissim tristique molestie. Donec vel massa nec turpis rhoncus lacinia.'
            }, {
                name: 'Éowyn',
                yearsInCompany: 5,
                image: {
                    src: '/assets/images/people/03.png',
                    alt: 'Éowyn photo'
                },
                profile: 'Vestibulum cursus nibh malesuada lobortis posuere. Nam vulputate at nisl nec commodo.'
            }, {
                name: 'Neo',
                yearsInCompany: 10,
                image: {
                    src: '/assets/images/people/01.png',
                    alt: 'Neo photo'
                },
                profile: 'Proin tempus ac libero a euismod. Nunc non blandit ipsum, a aliquet magna.'
            }, {
                name: 'Peter Parker',
                yearsInCompany: 1,
                image: {
                    src: '/assets/images/people/04.png',
                    alt: 'Peter Parker photo'
                },
                profile: 'Aenean eget fermentum nulla. Praesent lacinia finibus purus. Sed risus tortor, molestie at sollicitudin non, faucibus ut turpis. Pellentesque in ante vitae libero eleifend rhoncus.'
            }
        ];
    }

    updated(changedProperties) {
        if (changedProperties.has('people')) {
            this.dispatchEvent(
                new CustomEvent(
                    'get-people-data',
                    {
                        detail: {
                            people: this.people
                        }
                    }
                )
            );
        }
    }
}

customElements.define('persona-main-dm', PersonaMainDM);