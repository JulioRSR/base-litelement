import { LitElement, html } from "lit"

class PersonaStats extends LitElement {

    static get properties() {
        return {
            people: {type: Array}
        };
    }

    constructor() {
        super();

        this.people = [];
    }

    updated(changedProperties) {
        if (changedProperties.has('people')) {
            let peopleStats = this.gathePeopleArrayInfo(this.people);
            this.dispatchEvent(
                new CustomEvent(
                    'updated-people-stats',
                    {
                        detail: {
                            peopleStats: peopleStats
                        }
                    }
                )
            )
        }
    }

    gathePeopleArrayInfo(people) {
        let peopleStats = {};
        peopleStats.numberOfPeople = people.length;
        return peopleStats;
    }
}

customElements.define('persona-stats', PersonaStats);