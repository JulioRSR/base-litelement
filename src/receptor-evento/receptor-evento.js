import { LitElement, html } from "lit"

class ReceptorEvento extends LitElement {

    static get properties() {
        return {
            course: {type: String},
            year: {type: String}
        };
    }

    constructor() {
        super();
    }
    
    render() {
        return html`
            <h3>Receptor Evento</h3>
            <p>Este curso es de: ${this.course}</p>
            <p>Año: ${this.year}</p>
        `;
    }
}

customElements.define('receptor-evento', ReceptorEvento);