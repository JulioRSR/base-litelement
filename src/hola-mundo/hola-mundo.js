import { LitElement, html } from "lit";

class HolaMundo extends LitElement {
    
    render() {
        return html`
            <h1>Hola Mundo!</h1>
        `;
    }
}

customElements.define('hola-mundo', HolaMundo);