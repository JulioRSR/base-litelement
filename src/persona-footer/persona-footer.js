import { LitElement, html } from "lit"

class PersonaFooter extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <h5>PersonaApp Copyright ${this.getYear()}</h5>
        `;
    }

    getYear() {
        let year = new Date().getFullYear();
        return year;
    }
}

customElements.define('persona-footer', PersonaFooter);