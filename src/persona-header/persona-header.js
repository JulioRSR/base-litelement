import { LitElement, html } from "lit"

class PersonaHeader extends LitElement {

    static get properties() {
        return {
        };
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <h1>Header Persona APP</h1>
        `;
    }
}

customElements.define('persona-header', PersonaHeader);