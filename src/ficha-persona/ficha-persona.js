import { LitElement, html } from "lit"

class FichaPersona extends LitElement {

    static get properties() {
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            position: {type: String},
        };
    }

    constructor() {
        super();
        this.name           = 'John Doe';
        this.yearsInCompany = 12;
        this.updatePosition();
    }
    
    /**
     * Web component method, it belongs to component life cycle
     * @param {*} changedProperties 
     */
    updated(changedProperties) {
        //console.log('updated');
        changedProperties.forEach((oldValue, propName) => {
            //console.log('Propiedad '+ propName + ' cambia su valor, el valor anterior era ' + oldValue);
        });
        if (changedProperties.has('name')) {
            //console.log('Propiedad name cambia valor anterior era ' + changedProperties.get('name') + ' el nuevo es ' + this.name )
        }
        if (changedProperties.has('yearsInCompany')) {
            this.updatePosition();
        }
    }

    updateName(e) {
        //console.log('updateName');
        this.name = e.target.value;
    }

    updateYearsInCompany(e) {
        //console.log('updateYearsInCompany');
        this.yearsInCompany = e.target.value;
    }

    updatePosition() {
        //console.log('updatePosition');
        if (this.yearsInCompany >= 7) {
            this.position = 'lead';
        } else if (this.yearsInCompany >= 5) {
            this.position = 'senior';
        } else if (this.yearsInCompany >= 3) {
            this.position = 'team';
        } else {
            this.position = 'junior';
        }
    }

    render() {
        return html`
            <link rel="stylesheet" href="./src/ficha-persona/style.css">
            <div class="component-ficha-persona">
                <h1>Ficha Persona (Data Binding)</h1>
                <label>Nombre completo</label>
                <input type="text" value="${this.name}" @input="${this.updateName}">
                <br><br>
                <label>Años en la empresa</label>
                <input type="number" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}" min="1" max="15">
                <br><br>
                <label>Perfil</label>
                <input type="text" value="${this.position}" disabled>
            </div>
        `;
    }
}

customElements.define('ficha-persona', FichaPersona);